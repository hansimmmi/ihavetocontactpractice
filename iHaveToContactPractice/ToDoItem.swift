//
//  ToDoItem.swift
//  iHaveToContactPractice
//
//  Created by Philip Yun on 8/29/14.
//  Copyright (c) 2014 Philip Yun. All rights reserved.
//

import Foundation
import CoreData

@objc(TodoItem)
class ToDoItem: NSManagedObject {

    @NSManaged var identifier: String
    @NSManaged var note: String
    @NSManaged var dueDate: NSDate
    @NSManaged var contact: Contact

}
