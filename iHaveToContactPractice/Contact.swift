//
//  Contact.swift
//  iHaveToContactPractice
//
//  Created by Philip Yun on 8/29/14.
//  Copyright (c) 2014 Philip Yun. All rights reserved.
//

import Foundation
import CoreData

@objc(Contact)
class Contact: NSManagedObject {

    @NSManaged var email: String
    @NSManaged var firstName: String
    @NSManaged var lastName: String
    @NSManaged var identifier: String
    @NSManaged var phone: String
    @NSManaged var contactImage: NSData
    @NSManaged var toDoItem: NSSet

}
