//
//  ContactTableViewController.swift
//  iHaveToContactPractice
//
//  Created by Philip Yun on 9/5/14.
//  Copyright (c) 2014 Philip Yun. All rights reserved.
//

import UIKit
import CoreData


class ContactTableViewController: UITableViewController {

    
    
    var yourContacts = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        loadData()
        
    }
    
    
    func loadData() {
        yourContacts.removeAllObjects()
        var moc : NSManagedObjectContext = SwiftCoreDataHelper.managedObjectContext()
        var results = SwiftCoreDataHelper.fetchEntities(NSStringFromClass(Contact), withPredicate: nil, managedObjectContext: moc)
        
        for contact in results {
            var singleContact = contact as Contact
            var willBeAddedObject :NSDictionary = ["identifier":singleContact.identifier, "firstName":singleContact.firstName, "lastName":singleContact.lastName, "email":singleContact.email, "phone":singleContact.phone, "contactImage":singleContact.contactImage]
            
            yourContacts.addObject(willBeAddedObject)
        }
       
        self.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return yourContacts.count
    }

    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as ContactCellTableViewCell

        var contactDic = yourContacts.objectAtIndex(indexPath.row) as NSDictionary

        var firstName = contactDic.objectForKey("firstName") as NSString
        var lastName = contactDic.objectForKey("lastName") as NSString
        var email = contactDic.objectForKey("emaile") as NSString
        var phone = contactDic.objectForKey("phone") as NSString
        var contactImage = contactDic.objectForKey("contactImage") as NSData
        var contactImageFrame: UIImage = UIImage(data: contactImage)

        
        cell.nameLabel.text = firstName + " " + lastName
        cell.phoneLabel.text = phone
        cell.emailLabel.text = email
        
        var contactImageFrame2:CGRect = cell.contactImage.frame
        contactImageFrame2.size = CGSizeMake(75, 75)
        cell.contactImage.frame = contactImageFrame2
        cell.contactImage.image = contactImageFrame
        
        return cell
        
        
        
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
